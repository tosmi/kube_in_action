package main

import (
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type errorHandler struct {
	requestCount int
}

const dataFile = "/var/data/storage.txt"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		hostname, err := os.Hostname()

		if err != nil {
			//if err != nil {
			log.Fatalf("Could not determine hostname: %s", err)
		}
		fmt.Fprintf(w, "Hello %q, you've hit %s\n", html.EscapeString(r.URL.Path), hostname)

		if r.Method == "GET" {
			data, err := ioutil.ReadFile(dataFile)
			if err != nil {
				fmt.Fprintf(w, "Could not read  %s\n", dataFile)
				return
			}
			fmt.Fprintf(w, "data: %q", string(data))
		} else if r.Method == "POST" {
			data, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Fatal(w, "Could not read response")
			}

			err = ioutil.WriteFile(dataFile, data, 0644)
			if err != nil {
				fmt.Fprintf(w, "Could not write to data file %s", dataFile)
			}

		}
	})

	http.Handle("/error", &errorHandler{requestCount: 0})

	http.ListenAndServe(":8080", nil)
}

func (h *errorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	hostname, err := os.Hostname()
	if err != nil {
		//if err != nil {
		log.Fatalf("Could not determine hostname: %s", err)
	}

	if h.requestCount < 5 {
		fmt.Fprintf(w, "Hello %q, you've hit %s for the %v time\n", html.EscapeString(r.URL.Path), hostname, h.requestCount+1)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "500 - Sorry, you've hit %s more then 5 times\n", hostname)
	}

	h.requestCount++
}
