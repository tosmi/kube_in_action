package main

import (
	"fmt"
	"html"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
)

type errorHandler struct {
	requestCount int
}

const dataFile = "/var/data/storage.txt"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		hostname, err := os.Hostname()

		if err != nil {
			//if err != nil {
			log.Fatalf("Could not determine hostname: %s", err)
		}
		fmt.Fprintf(w, "Hello %q, you've hit %s\n", html.EscapeString(r.URL.Path), hostname)

		if r.Method == "GET" {
			peers, err := findPeers()
			fmt.Fprintf(w, "Found peers: %v", peers)
			getPeerData(peers, w)

			data, err := ioutil.ReadFile(dataFile)
			if err != nil {
				fmt.Fprintf(w, "Could not read %s: %v\n", dataFile, err)
				return
			}
			fmt.Fprintf(w, "data: %q", string(data))
		} else if r.Method == "POST" {
			data, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Fatal(w, "Could not read response")
			}

			err = ioutil.WriteFile(dataFile, data, 0644)
			if err != nil {
				fmt.Fprintf(w, "Could not write to data file %s", dataFile)
			}

		}
	})

	http.Handle("/error", &errorHandler{requestCount: 0})

	http.ListenAndServe(":8080", nil)
}

func findPeers() (peers []*net.SRV, err error) {
	cname, peers, err := net.LookupSRV("", "", "gosimple.default.svc.cluster.local")
	if err != nil {
		log.Print(err)
	}
	log.Printf("CNAME: %v, addrs: %v", cname, peers)
	return
}

func getPeerData(peers []*net.SRV, w io.Writer) {
	for _, p := range peers {
		url := fmt.Sprintf("http://%v:8080", p.Target)
		log.Printf("Connecting to %v", url)
		resp, err := http.Get(url)
		if err != nil {
			log.Printf("Contacting peer %v failed: %v", url, err)
			os.Exit(1)
		}
		log.Printf("response status: %v", resp.StatusCode)
		defer resp.Body.Close()
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Fprintf(w, "Reading response body failed: %v", err)
		}
		fmt.Fprint(w, string(b))
	}
}

func (h *errorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	hostname, err := os.Hostname()
	if err != nil {
		//if err != nil {
		log.Fatalf("Could not determine hostname: %s", err)
	}

	if h.requestCount < 5 {
		fmt.Fprintf(w, "Hello %q, you've hit %s for the %v time\n", html.EscapeString(r.URL.Path), hostname, h.requestCount+1)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "500 - Sorry, you've hit %s more then 5 times\n", hostname)
	}

	h.requestCount++
}
