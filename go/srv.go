package main

import (
	"fmt"
	"log"
	"net"
	"os"
)

func main() {
	log.Print("trying to resolv SRV record")
	name, addrs, err := net.LookupSRV("test", "tcp", "stderr.at")
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}

	for _, a := range addrs {
		fmt.Printf("addr: %v:%v\n", a.Target, a.Port)
	}

	log.Printf("name: %v, addrs: %v", name, addrs)
}
