#!/bin/bash

set -eof pipefail

while :
do
      echo $(date) Writing fortune to /var/htdocs/index.html
      /usr/bin/fortune > /var/htdocs/index.html
      sleep 10
done
