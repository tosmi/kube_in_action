simple: go/simple.go
	CGO_ENABLED=0 GOOS=linux go build -o docker/simple -a go/simple.go

stateful: go/simple_stateful.go
	CGO_ENABLED=0 GOOS=linux go build -o docker/gosimple-stateful/simple_stateful -a go/simple_stateful.go

srv: go/simple_stateful_srv.go
	CGO_ENABLED=0 GOOS=linux go build -o docker/gosimple-stateful/simple_stateful -a go/simple_stateful_srv.go

.PHONY: docker
docker-simple: simple
	(cd docker/gosimple; docker build -t tosmi/gosimple .)

.PHONY: docker-simple-push
docker-simple-push: docker-simple
	(cd docker/gosimple ; docker push tosmi/gosimple)

.PHONY: docker-stateful
docker-stateful: stateful
	(cd docker/gosimple-stateful; docker build -t tosmi/gosimple-stateful .)

.PHONY: docker-stateful-srv
docker-stateful-srv: srv
	(cd docker/gosimple-stateful; docker build -t tosmi/gosimple-stateful .)
	(cd docker/gosimple-stateful; docker push tosmi/gosimple-stateful)

.PHONY: docker-stateful-simple-push
docker-stateful-push: docker-stateful
	(cd docker/gosimple-stateful; docker push tosmi/gosimple-stateful)
